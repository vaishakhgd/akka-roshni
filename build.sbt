name := "AKKA-ROSHNI"

version := "0.1"

scalaVersion := "2.13.3"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies +=
  "com.typesafe.akka" %% "akka-actor" % "2.6.6"

libraryDependencies +=
  "com.typesafe.akka" %% "akka-remote" % "2.6.6"

libraryDependencies += "com.typesafe.akka" %% "akka-cluster-tools" % "2.6.6"

libraryDependencies += "com.typesafe.akka" %% "akka-cluster" % "2.6.6"

libraryDependencies += "com.typesafe.akka" %% "akka-cluster-typed" % "2.6.6"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime



