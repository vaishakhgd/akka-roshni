package chat2.chat

import akka.actor.Actor
import akka.cluster.pubsub.DistributedPubSub

class Publisher extends Actor {
  import akka.cluster.pubsub.DistributedPubSubMediator.Publish
  // activate the extension
  val mediator = DistributedPubSub(context.system).mediator
  mediator ! Publish("content", "YOOOOOOOOOO")

  def receive = {
    case in: String =>
      val out = in.toUpperCase
      mediator ! Publish("content", out)
  }
}
